from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from django.http import JsonResponse
from rest_framework.decorators import api_view

from User.models import User
from User.serializers import UserSerializer
# Create your views here.

@csrf_exempt
@api_view(['GET', 'POST'])
def RegistrationApi(request):
    if request.method == 'GET':
        return JsonResponse("GET API succefully", safe = False)

    elif request.method == 'POST':
        User_data=JSONParser().parse(request)
        User_serializer = UserSerializer(data=User_data)
        if User_serializer.is_valid():
            User_serializer.save()
            return JsonResponse("Added succefully", safe = False)
        return JsonResponse("FAILED", safe = False)
     
